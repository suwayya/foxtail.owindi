using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;
using StructureMap;

namespace FoxTail.OwinDi
{
    //The structure of the environment dictionary
    using IEnvironment = IDictionary<string, object>;

    //The structure of the "call next middleware" function.
    using NextMiddleware = Func<IDictionary<string, object>, Task>;


    /// <summary>
    /// Container middleware for the current dependency resolver. Manages scopes.
    /// </summary>
    internal class DependencyResolverMiddleware
    {
        private readonly NextMiddleware _next;
        private readonly IDependencyResolver _dependencyResolver;

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyResolverMiddleware"/> class.
        /// </summary>
        /// <param name="next">The next middleware.</param>
        /// <param name="container">The container.</param>
        public DependencyResolverMiddleware(NextMiddleware next, IContainer container)
        {
            _next = next;
            _dependencyResolver = new DependencyResolver(container);
        }


        /// <summary>
        /// Method used by owin to invoke the middleware.
        /// </summary>
        /// <param name="environment">The environment.</param>
        /// <returns></returns>
        public async Task Invoke(IEnvironment environment)
        {
            //Add the a new scope the current environment.
            environment.Add("foxtail.owindi.dependencyscope", _dependencyResolver.BeginScope());

            await _next.Invoke(environment);
            if (environment.ContainsKey("foxtail.owindi.dependencyscope"))
                return;

            //Dispose the scope from this environment.
            var scope = environment["foxtail.owindi.dependencyscope"] as IDependencyScope;
            if (scope != null)
                scope.Dispose();
        }
    }
}