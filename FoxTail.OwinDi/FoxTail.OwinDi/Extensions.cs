using System;
using System.Collections.Generic;
using System.Management.Instrumentation;
using System.Web.Http.Dependencies;
using Microsoft.Owin;
using Owin;
using StructureMap;

namespace FoxTail.OwinDi
{
    //The structure of the environment dictionary
    using IEnvironment = IDictionary<string, object>;

    public static class Extensions
    {

        /// <summary>
        /// Uses a structure map container.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="container">The container.</param>
        /// <remarks>This should always be first in the owin middleware chain.</remarks>
        /// <returns></returns>
        public static IAppBuilder UseStructureMapContainer(this IAppBuilder app, IContainer container)
        {
            //Add the dependency resolver middleware
            app.Use<DependencyResolverMiddleware>(container);
            return app;
        }

        /// <summary>
        /// Instanciates the middleware with dependencies per owin request.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="app">The application.</param>
        /// <returns></returns>
        public static IAppBuilder UseOwinRequestScoped<T>(this IAppBuilder app)
        {
            return app.Use<MiddlewareFactoryAdapter<T>>();
        }

        /// <summary>
        /// Retrieves a service from the resolver.
        /// </summary>
        /// <typeparam name="T">Type of the service</typeparam>
        /// <param name="dependencyResolver">The dependency resolver.</param>
        /// <returns></returns>
        public static T GetService<T>(this IDependencyResolver dependencyResolver)
        {
            return (T)dependencyResolver.GetService(typeof(T));
        }

        /// <summary>
        /// Retrieves a service from the scope.
        /// </summary>
        /// <typeparam name="T">Type of the service</typeparam>
        /// <param name="dependencyScope">The dependency scope.</param>
        /// <returns>The service.</returns>
        public static T GetService<T>(this IDependencyScope dependencyScope)
        {
            return (T)dependencyScope.GetService(typeof(T));
        }

        /// <summary>
        /// Retrieves a service from the scope if it exists in the contexts environment.
        /// </summary>
        /// <typeparam name="T">Type of the service</typeparam>
        /// <param name="context">The owin context.</param>
        /// <returns>The service.</returns>
        public static T GetService<T>(this IOwinContext context)
        {
            return (T)context.Environment.GetService(typeof(T));
        }

        /// <summary>
        /// Retrieves a service from the scope if it exists in the enviroment dictionary.
        /// </summary>
        /// <param name="environment">The environment.</param>
        /// <param name="type">The type if the service.</param>
        /// <returns>The service.</returns>
        /// <exception cref="System.Management.Instrumentation.InstanceNotFoundException">
        /// No scope found.
        /// </exception>
        internal static object GetService(this IEnvironment environment, Type type)
        {
            if (!environment.ContainsKey("foxtail.owindi.dependencyscope"))
                throw new InstanceNotFoundException("No scope found.");
            var scope = environment["foxtail.owindi.dependencyscope"] as IDependencyScope;
            if (scope == null)
                throw new InstanceNotFoundException("No scope found.");
            return scope.GetService(type);
        }
    }
}