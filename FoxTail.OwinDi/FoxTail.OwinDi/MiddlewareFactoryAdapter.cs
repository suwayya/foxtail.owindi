using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Owin;

namespace FoxTail.OwinDi
{
    //The structure of the environment dictionary
    using IEnvironment = IDictionary<string, object>;

    //The structure of the "call next middleware" function.
    using NextMiddleware = Func<IDictionary<string, object>, Task>;

    /// <summary>
    /// Acts as an adapter for middleware with dependencies.
    /// </summary>
    /// <typeparam name="T">Type of the middleware.</typeparam>
    internal class MiddlewareFactoryAdapter<T>
    {
        private readonly NextMiddleware _next;

        private ParameterInfo[] _ctorParameters;

        private MethodInfo _invokeMethod;

        private ParameterInfo _invokeMethodParameter;

        /// <summary>
        /// Initializes a new instance of the <see cref="MiddlewareFactoryAdapter{T}"/> class.
        /// </summary>
        /// <param name="next">The next.</param>
        public MiddlewareFactoryAdapter(NextMiddleware next)
        {
            _next = next;
            ValidateMiddleware();
        }


        /// <summary>
        /// Validates the middleware.
        /// </summary>
        /// <exception cref="System.MissingFieldException">
        /// No Invoke method found.
        /// or
        /// Invoke method with IDictionary<string, object> as only parameter not found.
        /// or
        /// No constructors found.
        /// </exception>
        /// <exception cref="System.NotSupportedException">
        /// Multiple Invoke methods not supported.
        /// or
        /// Return types other then Task is not supported.
        /// or
        /// Multiple constructors not supported.
        /// </exception>
        private void ValidateMiddleware()
        {
            var t = typeof(T);
            //Check if we have a valid Invoke method.
            var methods = t.GetMethods().Where(m => m.Name == "Invoke").ToArray();
            if (!methods.Any())
                throw new MissingFieldException("No Invoke method found.");
            if (methods.Count() > 1)
                throw new NotSupportedException("Multiple Invoke methods not supported.");
            _invokeMethod = methods.First();
            if (_invokeMethod.ReturnType != typeof(Task))
                throw new NotSupportedException("Return types other then Task is not supported.");
            var invokeMethodParameters = _invokeMethod.GetParameters();
            if (invokeMethodParameters.Count() == 1
                && invokeMethodParameters.All(m => m.ParameterType != typeof (IEnvironment)))
                throw new MissingFieldException("Invoke method with IDictionary<string, object> as only parameter not found.");
            _invokeMethodParameter = invokeMethodParameters.First();
            //Please guys, just one constructor.
            var ctors = t.GetConstructors();
            if (!ctors.Any())
                throw new MissingFieldException("No constructors found.");
            if (ctors.Count() > 1)
                throw new NotSupportedException("Multiple constructors not supported.");
            var ctor = ctors.First();
            var parameters = ctor.GetParameters();
            _ctorParameters = parameters;
        }


        /// <summary>
        /// Method used by owin to invoke the middleware.
        /// </summary>
        /// <param name="environment">The environment.</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException">Invoke method only supports IDictionary<string, object>.</exception>
        public async Task Invoke(IEnvironment environment)
        {
            
            //Create the middleware
            var instance = Create(environment);

            //Prepp the parameter to be passed to the invoke method.
            object invokeParameter = null;
            if (_invokeMethodParameter.ParameterType == typeof (IEnvironment))
                invokeParameter = environment;

            //Invoke it!
            await (Task)_invokeMethod.Invoke(instance, new []{ invokeParameter });
        }

        /// <summary>
        /// Creates the middleware instance.
        /// </summary>
        /// <param name="environment">The environment.</param>
        /// <returns></returns>
        public T Create(IEnvironment environment)
        {
            var ctorParameters = new object[_ctorParameters.Length];
            //Find the dependencies.
            foreach (var ctorParameter in _ctorParameters)
            {
                if (ctorParameter.ParameterType.IsAssignableFrom(typeof(NextMiddleware)))
                    ctorParameters[ctorParameter.Position] = _next;
                else
                    ctorParameters[ctorParameter.Position] = environment.GetService(ctorParameter.ParameterType);
            }

            //Create the middleware with dependencies.
            return (T)Activator.CreateInstance(typeof(T), ctorParameters);
        }
    }
}