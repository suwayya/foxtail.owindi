﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using FoxTail.OwinDi;
using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Owin;
using StructureMap;

namespace OwinDiTests
{
    using NextMiddleware = Func<IDictionary<string, object>, Task>;

    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public async Task TestMiddlewareDependency()
        {
            using (var server = TestServer.Create<AppStart>())
            {
                HttpResponseMessage response = await server.HttpClient.GetAsync("/");

                //Execute necessary tests
                Assert.AreEqual("Hello OWIN DI!", await response.Content.ReadAsStringAsync());
            }
        }
    }

    public class AppStart
    {
        public void Configuration(IAppBuilder app)
        {
            var container = new Container();
            container.Configure(config =>
            {
                config.For<ITestInterface>().Use<TestImplementation>();
            });
            app.UseStructureMapContainer(container);
            app.UseOwinRequestScoped<MiddlewareWithDependencies>();
            app.Run(async context =>
            {
                context.Response.WriteAsync(context.Environment["TEST"].ToString());
            });
        }
    }

    public class MiddlewareWithDependencies
    {
        private readonly NextMiddleware _next;

        private readonly ITestInterface _testDependency;

        public MiddlewareWithDependencies(NextMiddleware next, ITestInterface testDependency)
        {
            _next = next;
            _testDependency = testDependency;
        }

        public async Task Invoke(IDictionary<string, object> env)
        {
            env.Add("TEST", _testDependency.Value);
            await _next.Invoke(env);
        }
    }

    public interface ITestInterface
    {
        string Value { get; }
    }

    public class TestImplementation : ITestInterface
    {
        public string Value
        {
            get { return "Hello OWIN DI!"; }
        }
    }
}
