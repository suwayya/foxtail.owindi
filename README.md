# OwinDi #
Dependency injection in OWIN with structuremap.

## Example Use ##

```
#!c#

public void Configuration(IAppBuilder app)
{
    var container = new Container();
    container.Configure(config =>
    {
        config.For<ITestInterface>().Use<TestImplementation>();
    });
    //Adds a middleware that handles scopes per request.
    app.UseStructureMapContainer(container);

    //Add a custom middleware that will be injected with dependencies per OWIN request
    app.UseOwinRequestScoped<MiddlewareWithDependencies>();
    app.Run(async context =>
    {
        context.Response.WriteAsync(context.Environment["TEST"].ToString());
    });
}
```


```
#!c#

public class MiddlewareWithDependencies
{
    private Func<IDictionary<string, object>, Task> _next;

    private ITestInterface _testDependency;

    public MiddlewareWithDependencies(Func<IDictionary<string, object>, Task> next, ITestInterface testDependency)
    {
        _next = next;
        _testDependency = testDependency;
    }

    public async Task Invoke(IDictionary<string, object> env)
    {
        env.Add("TEST", _testDependency.Value);
        await _next.Invoke(env);
    }
}

public interface ITestInterface
{
    string Value { get; }
}

public class TestImplementation : ITestInterface
{
    public string Value
    {
        get { return "Hello OWIN DI!"; }
    }
}
```